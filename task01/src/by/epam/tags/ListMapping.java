package by.epam.tags;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.TagSupport;
import java.util.*;


public class ListMapping extends TagSupport {
    private Queue<? extends ServletRegistration> queueServletRegistrations;

    @Override
    public int doStartTag() {
        Map<String, ? extends ServletRegistration> servletRegistrations = pageContext.getServletContext().getServletRegistrations();
        queueServletRegistrations = new LinkedList<ServletRegistration>(servletRegistrations.values());
        return EVAL_BODY_INCLUDE;
    }

    @Override
    public int doAfterBody() {
        while (queueServletRegistrations.size() > 0) {
            ServletRegistration servletRegistration = queueServletRegistrations.element();
            pageContext.setAttribute("url", servletRegistration.getMappings());
            pageContext.setAttribute("servlet", servletRegistration.getName());
            queueServletRegistrations.poll();
            return EVAL_BODY_AGAIN;
        }
        return SKIP_BODY;
    }
}
