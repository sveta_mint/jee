package by.epam.tags;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ResolveUrl extends SimpleTagSupport {
    private String url;
    private String resource;
    private static Logger logger = Logger.getLogger(ResolveUrl.class.getName());

    @Override
    public void doTag() {
        getJspContext().setAttribute("url", url);
        ServletContext servletContext = ((PageContext) getJspContext()).getServletContext();
            resource = servletContext.getRealPath(url);
        if (resource != null) {
            getJspContext().setAttribute("resource", resource);
        }
        final StringWriter stringWriter = new StringWriter();
        try {
            getJspBody().invoke(stringWriter);
            getJspContext().getOut().println(stringWriter);
        } catch (JspException  | IOException e) {
            logger.log(Level.SEVERE, "something was wrong! Error: {0}", new Object[]{e.getMessage()});
        }
    }
	
	public void setUrl(String url) {
        this.url = url;
    }
}
