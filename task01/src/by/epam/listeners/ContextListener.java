package by.epam.listeners;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRegistration;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Class for context listener
 */
public class ContextListener implements ServletContextListener {
    @Override
    public void contextInitialized(final ServletContextEvent servletContextEvent) {
        Logger logger = Logger.getLogger("listener");
        ServletContext servletContext = servletContextEvent.getServletContext();
        logger.info("Name application: " + servletContextEvent.getServletContext().getServletContextName());
        Map<String, ? extends ServletRegistration> servletRegistrations = servletContext.getServletRegistrations();
        for (String key : servletRegistrations.keySet()) {
            ServletRegistration servletRegistration = servletRegistrations.get(key);
            logger.info("Name servlet: " + servletRegistration.getName()
                    + " Servlet url: " + servletRegistration.getMappings());
        }
    }

    @Override
    public void contextDestroyed(final ServletContextEvent servletContextEvent) {

    }
}
