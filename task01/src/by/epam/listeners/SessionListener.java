package by.epam.listeners;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.*;
import java.util.logging.Logger;

/**
 * Class for session listener
 */
@WebListener()
public class SessionListener implements HttpSessionListener {
    private static Logger logger = Logger.getLogger(SessionListener.class.getName());
    /**
     * Method for getting ID and session creation time
     * @param se HttpSessionEvent
     */
    public void sessionCreated(final HttpSessionEvent se) {
        HttpSession session = se.getSession();
        logger.info("Session id: " + session.getId() + " was created " + session.getCreationTime());
    }

    /**
     * Method for getting ID and session destroy time
     * @param se HttpSessionEvent
     */
    public void sessionDestroyed(final HttpSessionEvent se) {
        HttpSession session = se.getSession();
        logger.info("Session id: " + session.getId() + " was destroyed " + System.currentTimeMillis());
    }


}
