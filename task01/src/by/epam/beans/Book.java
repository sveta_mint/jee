package by.epam.beans;

import java.util.Objects;

/**
 *Description of the entity Book
 */
public class Book {
    private Integer id;
    private String name;
    private String author;
    private String description;

    /**
     * Constructor of the entity Book
     * @param id primary key
     * @param name name of book
     * @param author author of book
     * @param description description of book
     */
    public Book(final Integer id, final String name, final String author, final String description) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.description = description;
    }

    /**
     * Setter for param name
     * @param name name of book
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Setter for param author
     * @param author author of book
     */
    public void setAuthor(final String author) {
        this.author = author;
    }

    /**
     * Setter for param description
     * @param description description of book
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Getter for primary key
     * @return primary key
     */
    public Integer getId() {
        return id;
    }

    /**
     * Setter for primary key
     * @param id primary key
     */
    public void setId(final Integer id) {
        this.id = id;
    }

    /**
     * Getter for name
     * @return name book
     */
    public String getName() {
        return name;
    }

    /**
     * Getter for author
     * @return author of book
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Getter for description
     * @return description of book
     */
    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return Objects.equals(getId(), book.getId()) &&
                Objects.equals(getName(), book.getName()) &&
                Objects.equals(getAuthor(), book.getAuthor()) &&
                Objects.equals(getDescription(), book.getDescription());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "Book: " + "id= " + id + ", name= " + name + ", author= " + author + ", description= " + description;
    }
}
