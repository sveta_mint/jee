package by.epam.filters;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Pattern;
/**
 * FilterRequestWrapper for XssFilter
 *
 */
public class FilterRequestWrapper extends HttpServletRequestWrapper {
    private static final List<Pattern> UNSECURED_LIST = new ArrayList<>(Arrays.asList(Pattern.compile("<script>"),
            Pattern.compile("</script>"), Pattern.compile("<script>(.*?)</script>")));

    private String protectedInputValue(final String input) {
        String result = null;
        for (Pattern unsecuredPattern : UNSECURED_LIST) {
            result = unsecuredPattern.matcher(input).replaceAll("");
        }
        return result;
    }

    @Override
    public String getParameter(final String name) {
        final String inputValue = super.getParameter(name);
        return protectedInputValue(inputValue);
    }

    @Override
    public String[] getParameterValues(final String name) {
        final String[] inputValues = super.getParameterValues(name);
        if (inputValues != null) {
            for (int i = 0; i < inputValues.length; i++) {
                inputValues[i] = protectedInputValue(inputValues[i]);
            }
        }
        return inputValues;
    }

    @Override
    public String getHeader(final String name) {
        final String value = super.getHeader(name);
        return protectedInputValue(value);
    }

    /**
     * Constructor to wrap the request
     * @param request HttpServletRequest for request wrap
     */
    public FilterRequestWrapper(final HttpServletRequest request) {
        super(request);
    }
}
