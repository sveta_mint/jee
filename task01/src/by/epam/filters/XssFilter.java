package by.epam.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * name's filter Xsfilter
 */
@WebFilter(filterName = "XssFilter")
public class XssFilter implements Filter {
    /**
     * Method of termination of the filter
     */
    public void destroy() {
    }

    /**
     * The method of operation of the filter
     * @param req request
     * @param resp response
     * @param chain filterChain object
     * @throws ServletException exception
     * @throws IOException exception
     */
    public void doFilter(final ServletRequest req, final ServletResponse resp, final FilterChain chain)
            throws ServletException, IOException {
        chain.doFilter(new FilterRequestWrapper((HttpServletRequest) req), resp);
    }

    /**
     * The configuration of the filter
     * @param config FilterConfig object
     * @throws ServletException exception
     */
    public void init(final FilterConfig config) throws ServletException {

    }

}
