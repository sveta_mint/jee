package by.epam.connection;

import java.sql.*;
import java.util.ResourceBundle;

/**
 * MySql connection
 */
public class MySqlConnectionUtils {
    /**
     * method for getting connection to db
     * @return connection
     */
    public static Connection getConnection() {
        ResourceBundle resource = ResourceBundle.getBundle("config");
        String url = resource.getString("db.url");
        String driver = resource.getString("db.driver");
        String user = resource.getString("db.user");
        String pass = resource.getString("db.password");
        try {
            Class.forName(driver);
            return DriverManager.getConnection(url, user, pass);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
