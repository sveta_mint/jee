package by.epam.connection;

/**
 * Constants for app
 */
final public class Constants {
    public static final String TEMPORARY_UNAVAILABLE = "temporaryUnavailable";
    public static final String PERMANENT_UNAVAILABLE = "permanentUnavailable";
    public static final String FLAG_SERVLET = "FlagServlet";
    public static final String TIME_UNAVAILABLE = "TimeUnavailable";
    public static final String ERROR_MESSAGE_TEMPORARY = "Servlet temporary Unavailable";
    public static final String ERROR_MESSAGE_PERMANENT = "Servlet permanent Unavailable";
    public static final String ERROR_SQL_MESSAGE = "Some problem with connection";
    public static final String ERROR_EMPTY_FIELDS = "The input fields are empty!";

    public static final String JUMP_BOOK = "/books.jsp";
    public static final String JUMP_MAIN = "/index.jsp";
    public static final String ADD_BOOK = "/addBook.jsp";
    public static final String EDITBOOK = "/editBook.jsp";
    public static final String KEY_BOOKS = "books";
    public static final String KEY_BOOK = "book";
    public static final String KEY_ID = "id";
    public static final String KEY_NAME = "name";
    public static final String KEY_AUTHOR = "author";
    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_ERROR_MESSAGE = "errorMessage";


    public static final String SELECT_BOOK = "Select id,name, author, description from books";
    public static final String INSERT_BOOK = "Insert into books(name,author,description) values (?,?,?)";
    public static final String EDIT_BOOK = "Update books set author=?,description=? where books.id=?";
    public static final String FIND_BOOK = "Select id, name, author, description from books where id= ?";
    private Constants() {
    }
}
