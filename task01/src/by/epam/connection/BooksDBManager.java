package by.epam.connection;

import by.epam.beans.Book;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Book operations
 */
public class BooksDBManager {
    /**
     * Method for finding all books
     * @return list of book
     */
    public List<Book> findAllBooks() {
        List<Book> books = new ArrayList<>();
        try (Connection connection = MySqlConnectionUtils.getConnection();
             PreparedStatement ps = connection.prepareStatement(Constants.SELECT_BOOK);
        ) {
            try (ResultSet rs = ps.executeQuery();) {
                Book book;
                while (rs.next()) {
                    book = new Book(rs.getInt(Constants.KEY_ID),
                            rs.getString(Constants.KEY_NAME),
                            rs.getString(Constants.KEY_AUTHOR),
                            rs.getString(Constants.KEY_DESCRIPTION));
                    books.add(book);
                }
            }
        } catch (SQLException e) {
            System.err.println(Constants.ERROR_SQL_MESSAGE + "\n" + e);
        }
        return books;
    }

    /**
     * method for adding book
     * @param name name of book
     * @param author author of book
     * @param description description of book
     */
    public void addBook(final String name, final String author, final String description) {
        try (Connection connection = MySqlConnectionUtils.getConnection();
             PreparedStatement ps = connection.prepareStatement(Constants.INSERT_BOOK)) {
            final int NAME = 1, AUTHOR = 2, DESCRIPTION = 3;
            ps.setString(NAME, name.trim());
            ps.setString(AUTHOR, author.trim());
            ps.setString(DESCRIPTION, description.trim());
            ps.executeUpdate();
        } catch (SQLException e) {
            System.err.println(Constants.ERROR_SQL_MESSAGE + "\n" + e);
        }
    }

    /**
     * Method for editing book
     * @param id primary key
     * @param author author of book
     * @param description description of book
     */
    public void editBook(final int id, final String author, final String description) {
        try (Connection connection = MySqlConnectionUtils.getConnection();
             PreparedStatement ps = connection.prepareStatement(Constants.EDIT_BOOK)) {
            final int ID = 3, AUTHOR = 1, DESCRIPTION = 2;
            ps.setInt(ID, id);
            ps.setString(AUTHOR, author);
            ps.setString(DESCRIPTION, description);
            ps.executeUpdate();
        } catch (SQLException e) {
            System.err.println(Constants.ERROR_SQL_MESSAGE + "\n" + e);
        }
    }

    /**
     * Method for finding book by id
     * @param id primary key
     * @return book
     */
    public Book findBookById(final int id) {
        try (Connection connection = MySqlConnectionUtils.getConnection();
            PreparedStatement ps = connection.prepareStatement(Constants.FIND_BOOK)) {
            final int ID = 1;
            ps.setInt(ID, id);
           try(ResultSet rs = ps.executeQuery();) {
               Book book;
               while (rs.next()) {
                   book = new Book(rs.getInt(Constants.KEY_ID),
                           rs.getString(Constants.KEY_NAME),
                           rs.getString(Constants.KEY_AUTHOR),
                           rs.getString(Constants.KEY_DESCRIPTION));
                   return book;
               }
           }
        } catch (SQLException e) {
            System.err.println(Constants.ERROR_SQL_MESSAGE + "\n" + e);
        }
        return null;
    }
}
