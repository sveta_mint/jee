package by.epam.controllers;

import by.epam.beans.Book;
import by.epam.connection.BooksDBManager;
import by.epam.connection.Constants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * name edit book controller
 */
@WebServlet(name = "EditBookController")

public class EditBookController extends AbstractBaseController {
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter(Constants.KEY_ID);
        String errorMessage = null;
        BooksDBManager booksDBManager = new BooksDBManager();
        Book book = booksDBManager.findBookById(Integer.parseInt(id));
        if (errorMessage != null && book == null) {
            forwardError(Constants.EDITBOOK, errorMessage, request, response);
        }
        request.setAttribute(Constants.KEY_ERROR_MESSAGE, errorMessage);
        request.setAttribute(Constants.KEY_BOOK, book);
        forward(Constants.EDITBOOK, request, response);
    }
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        String idBook = request.getParameter(Constants.KEY_ID);
        String author = request.getParameter(Constants.KEY_AUTHOR);
        String description = request.getParameter(Constants.KEY_DESCRIPTION);
        String inputValues = checkUserInput(idBook, author, description);
        if (inputValues != null) {
            forwardError(Constants.EDITBOOK, inputValues, request, response);
        }
        BooksDBManager booksDBManager = new BooksDBManager();
        booksDBManager.editBook(Integer.parseInt(idBook), author, description);
        forward(Constants.JUMP_MAIN, request, response);
    }

}
