package by.epam.controllers;

import by.epam.beans.Book;
import by.epam.connection.BooksDBManager;

import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static by.epam.connection.Constants.*;

/**
 * name of view book controller
 */
@WebServlet(name = "ViewBookController")
public class ViewBookController extends AbstractBaseController {
     protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
             throws ServletException, IOException {
         String flag = getInitParameter(FLAG_SERVLET);
         if (isFailServlet()) {
             if (flag.equals(TEMPORARY_UNAVAILABLE)) {
                 throw new UnavailableException(ERROR_MESSAGE_TEMPORARY,
                         Integer.parseInt(getInitParameter(TIME_UNAVAILABLE)));
             }
             if (flag.equals(PERMANENT_UNAVAILABLE)) {
                 throw new UnavailableException(ERROR_MESSAGE_PERMANENT);
             }
         }
         BooksDBManager booksDBManager = new BooksDBManager();
         List<Book> books = booksDBManager.findAllBook();
         request.setAttribute(KEY_BOOKS, books);
         forward(JUMP_BOOK, request, response);
     }
     private boolean isFailServlet() {
         boolean fail = false;
         if ((int) (Math.random() * 100) % 2 == 0) {
             fail = true;
         }
         return fail;
     }
}
