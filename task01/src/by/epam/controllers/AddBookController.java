package by.epam.controllers;

import by.epam.connection.BooksDBManager;
import by.epam.connection.Constants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * name of add book controller
 */
@WebServlet(name = "AddBookController")
/**
 * Add book controller
 */
public class AddBookController extends AbstractBaseController {
    private BooksDBManager booksDBManager;
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        String name = request.getParameter(Constants.KEY_NAME);
        String author = request.getParameter(Constants.KEY_AUTHOR);
        String description = request.getParameter(Constants.KEY_DESCRIPTION);
        String inputValues = checkUserInput(name, author, description);
        if (inputValues != null) {
            forwardError(Constants.ADD_BOOK, inputValues, request, response);
        }
        booksDBManager = new BooksDBManager();
        booksDBManager.addBook(name, author, description);
        forward(Constants.JUMP_MAIN, request, response);
    }

}
