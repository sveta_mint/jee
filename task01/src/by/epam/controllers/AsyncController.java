package by.epam.controllers;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * Async controller
 */
@WebServlet(name = "AsyncController", asyncSupported = true)
public class AsyncController extends AbstractBaseController {
    private static Logger logger = Logger.getLogger(AsyncController.class.getName());
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        AsyncContext asyncContext = request.startAsync();
        asyncContext.start(() -> {
            try {
                final long start = System.currentTimeMillis();
                Thread.sleep(2000);
                if (request.isAsyncStarted()) {
                    asyncContext.dispatch("/dispatch");
                    request.setAttribute("startAsync", System.currentTimeMillis() - start);
                }
            } catch (InterruptedException e) {
                logger.info("Something was wrong: " + e.toString());
            }
        });
    }
}
