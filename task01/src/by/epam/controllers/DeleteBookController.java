package by.epam.controllers;

import by.epam.beans.Book;
import by.epam.connection.BooksDBManager;
import by.epam.connection.Constants;

import javax.servlet.ServletException;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(name = "DeleteBookController", urlPatterns = {"/deleteBook"})
@ServletSecurity(httpMethodConstraints = {
        @HttpMethodConstraint(value = "POST", rolesAllowed = "user_management")
})
public class DeleteBookController extends AbstractBaseController {
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        String idBook = request.getParameter(Constants.KEY_ID);
        String errorMessage = Constants.ERROR_MESSAGE;
        BooksDBManager booksDBManager = new BooksDBManager();
        Book book = booksDBManager.findBookById(Integer.parseInt(idBook));
        if(book == null) {
            request.setAttribute(Constants.KEY_ERROR_MESSAGE, errorMessage);
            forwardError(Constants.DELETEBOOK, errorMessage, request,response);
        }
        booksDBManager.deleteBook(Integer.parseInt(idBook));
        forward(Constants.JUMP_MAIN, request, response);
    }
}
