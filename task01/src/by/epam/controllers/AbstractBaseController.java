package by.epam.controllers;

import by.epam.connection.Constants;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * name controller
 */
@WebServlet(name = "AbstractBaseController")
/**
 * Base controller
 */
public class AbstractBaseController extends HttpServlet {
    protected void forward(final String url, final HttpServletRequest request,
                           final HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = getServletContext().getRequestDispatcher(url);
        rd.forward(request, response);
    }
    protected void forwardError(final String url, final String message, final HttpServletRequest request,
                                final HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute(Constants.KEY_ERROR_MESSAGE, message);
        forward(url, request, response);
    }

    protected String checkUserInput(String name, String author, String  description) {
        name = name.trim();
        author = author.trim();
        description = description.trim();
        if ("".equals(name) || "".equals(author) || "".equals(description)) {
            return Constants.ERROR_EMPTY_FIELDS;
        }
        return null;
    }
}
