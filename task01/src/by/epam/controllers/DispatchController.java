package by.epam.controllers;

import by.epam.connection.Constants;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * dispatch controller for async
 */
@WebServlet(name = "DispatchController", asyncSupported = true)
public class DispatchController extends AbstractBaseController {
    private static Logger logger = Logger.getLogger(DispatchController.class.getName());
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        AsyncContext asyncContext = request.startAsync();
        asyncContext.start(() -> {
            try {
                final long start = System.currentTimeMillis();
                Thread.sleep(5000);
                request.setAttribute("dispatch", System.currentTimeMillis() - start);
                forward(Constants.VIEW_ASYNC_RESULT, request, response);
            } catch (InterruptedException | ServletException | IOException e) {
                logger.info("Something was wrong: " + e.toString());
            } finally {
                asyncContext.complete();
            }
        });
    }
}
