<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Async Result</title>
</head>
<body>
<h3>Async result</h3>
<div>
     <p>First attribute is <c:out value="${startAsync}"/> ms</p>
     <p>Second attribute is  <c:out value="${dispatch}"/> ms</p>
</div>
</body>
</html>
