<%@ page import="by.epam.connection.Constants" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <Title>Add book</Title>
<body>
    <div>
        <h3>Enter new book, then click Add</h3>
        <form name="addForm" method="POST" action="<c:url value='/addBook'/>">
            <c:if test="${not empty errorMessage}">
                <c:out value="${errorMessage}"/><br>
            </c:if>
            <strong>Name book</strong>
            <input type="text" name=<%= Constants.KEY_NAME%>><br>
            <strong>Author</strong>
            <input type="text" name=<%= Constants.KEY_AUTHOR %>><br>
            <strong>Description</strong>
            <input type="text" name=<%= Constants.KEY_DESCRIPTION %>><br>
            <input type="submit" value="Add">
        </form>
    </div>
</body>
</html>