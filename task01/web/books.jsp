<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="by.epam.connection.Constants" %>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<%@ taglib prefix="util" uri="utils.tag" %>
<html>
<head>
    <title>Book</title>
</head>
<body>
<h3>Show ResolveUrlTag</h3>
<util:resolveurl url="/init.properties">
    ${url} - ${resource}
</util:resolveurl>
<h3>Book list</h3>
<c:if test="${not empty errorMessage}">
    <c:out value="${errorMessage}"/><br>
</c:if>
<form name="books" method="POST">
<table border="1" cellpadding="5" cellspacing="1" >
    <tr>
        <th>id</th>
        <th>Name</th>
        <th>Author</th>
        <th>Description</th>
        <th>Edit</th>
    </tr>
    <c:forEach items="${books}" var="book" >
        <tr>
            <td><c:out value="${book.id}"/></td>
            <td><c:out value="${book.name}"/></td>
            <td><c:out value="${book.author}"/></td>
            <td><c:out value="${book.description}"/></td>
            <td>
                <a href="<c:url value='/edit' ><c:param name='id' value="${book.id}" /></c:url>">Edit</a>
            </td>
            <c:if test="${initParam.isPluggability eq true}">
                <td>
                    <a href="<c:url value='/upload' ><c:param name='id' value="${book.id}" /></c:url>">Upload cover</a>
                </td>
            </c:if>
        </tr>
    </c:forEach>
</table>
<a href=<%= Constants.ADD_BOOK %>>Add new book</a>
<a href="<c:url value='/async' />">View Async Result</a>
<a href=<%= Constants.DELETEBOOK %>>Security page</a>
</form>
</body>
</html>

