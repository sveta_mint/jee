<%@ page import="by.epam.connection.Constants" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<html>
<head>
    <title>Edit book</title>
</head>
<body>
<div>
    <h3>Edit author and description, then click Save</h3>
        <form name="editForm" method="POST" action="<c:url value='/editBook'/>">
            <c:if test="${not empty errorMessage}">
                <c:out value="${errorMessage}"/><br>
            </c:if>
            <strong>idBook</strong>
            <input type="text" value=<c:out value="${book.id}"/> name=<%= Constants.KEY_ID %>><br>
            <strong>Author</strong>
            <input type="text" value=<c:out value="${book.author}"/> name=<%= Constants.KEY_AUTHOR %>><br>
            <strong>Description</strong>
            <input type="text" value=<c:out value="${book.description}"/> name=<%= Constants.KEY_DESCRIPTION %>><br>
            <input type="submit" value="Save" />
        </form>
</div>
</body>
</html>