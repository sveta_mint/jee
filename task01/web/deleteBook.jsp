<%@ page import="by.epam.connection.Constants" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<html>
<head>
    <title>Delete book</title>
</head>
<body>
<div>
    <h3>Enter idBook, then click Delete</h3>
    <form name="deleteForm" method="POST" action="<c:url value='/deleteBook'/>">
        <c:if test="${not empty errorMessage}">
            <c:out value="${errorMessage}"/><br>
        </c:if>
        <strong>idBook</strong>
        <input type="text" name=<%= Constants.KEY_ID %>><br>
        <input type="submit" value="Delete" />
    </form>
</div>
</body>
</html>