package by.epam.filters;


import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebFilter("/upload")
public class LogRequestUploadFilter implements Filter {
    private final static Logger LOGGER = Logger.getLogger(LogRequestUploadFilter.class.getName());
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        LOGGER.log(Level.INFO, "Get the IP address of user {0}", new Object[]{req.getRemoteAddr()} );
        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
