package by.epam.controllers;

import by.epam.beans.Book;
import by.epam.connection.BooksDBManager;
import by.epam.connection.Constants;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebServlet(name = "UploadController", urlPatterns = {"/upload"},
        initParams = @WebInitParam(name = "mimeType", value = "image/jpeg"))
@MultipartConfig
public class UploadController extends HttpServlet {
    private final static Logger LOGGER = Logger.getLogger(UploadController.class.getName());

    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        try {
            int id = Integer.parseInt(request.getParameter("id"));
            forward("/upload.jsp", request, response);
        } catch (NumberFormatException e) {
            forwardError("Book not found!", request, response);
            LOGGER.log(Level.SEVERE, "Book not found: {0}", new Object[]{e.getMessage()});
        }
    }

    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        final String path = request.getParameter("destination");
        final Part filePart = request.getPart("file");
        final String fileName = filePart.getSubmittedFileName();
        final String mimeType = getServletConfig().getInitParameter("mimeType");

        if (filePart == null || path == null || !mimeType.equals(filePart.getContentType())) {
            forwardError("Wrong type uploading file or file not select!", request, response);
        }
        try (OutputStream out = new FileOutputStream(new File(path + File.separator
                    + fileName)); InputStream fileContent = filePart.getInputStream()) {
            int read = 0;
            final byte[] bytes = new byte[1024];
            while ((read = fileContent.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            LOGGER.log(Level.INFO, "File {0} being uploaded to {1}", new Object[]{fileName, path});
        } catch (FileNotFoundException e) {
            LOGGER.log(Level.SEVERE, "Some problems with upload file! Error: {0}", new Object[]{e.getMessage()});
        }
    }

    protected void forward(final String url, final HttpServletRequest request,
                           final HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = getServletContext().getRequestDispatcher(url);
        rd.forward(request, response);
    }

    private void forwardError(final String message, final HttpServletRequest request,
        final HttpServletResponse response) throws ServletException, IOException {
            request.setAttribute(Constants.KEY_ERROR_MESSAGE, message);
            forward("/upload.jsp", request, response);
    }

}
