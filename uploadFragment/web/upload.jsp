<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld" %>
<html>
<head>
  <title>Upload cover</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<form method="POST" enctype="multipart/form-data" action="<c:url value="/upload"/>">
  <c:if test="${not empty errorMessage}">
    <c:out value="${errorMessage}"/><br>
  </c:if>
  Image:
  <input type="file" name="file" /> <br>
  Save to:
  <input type="text"  name="destination" placeholder="C:/123data"/><br>
  <input type="submit" value="Upload" name="upload"  />
</form>
</body>
</html>